CHERRY BOMB  


SUPPLIES:  

Magnetic Whiteboard: Draw a Grid & col/row labels in sharpee. Put up next to the timesheets on the wall of the breakroom.  
 
Magnet "Tanks": Sized to fit a cell on the Board. Chunk of whiteboard glued on top to write player info??  

Bottle Caps: Act as Cherry points. Each player grabs one at clock-in. Maybe keep them in a paper cup in your locker.  
(caps that fall to the floor can be collected and used as cherry points, instead of being thrown away)  

Box: A closable/reopenable box with a hole cut in the top large enough to fit a Cherry cap.  

small notepad & Pen: Players write their action(s) on a page in the pad, tear it out and insert it into the Box along with their caps.  


THE CHERRYMEISTER: One player will be charged with accounting all player actions for the day, updating the board, and general managing of the game. The Cherrymeister takes the contents of the Box (at the end of the day) and accounts for each note left in the Box. Tanks are moved across the board, Hearts are taken away, points are traded, and dead Players are removed and placed in jury duty. 

THE BOARD: The game board is a (10x10?) Grid with letters across columns and numbers down rows. Designations for cells on the board are Battleship style: A5, F9, etc.  

THE TANK: Each player has a Cherry Tank that exists in one cell of the board. Only one player may occupy a cell, and Tanks cannot move through occupied cells. The Board and placement and status of all Tanks is visible to everyone.
HEARTS: Each tank has 3 Hearts. Each Cherry you are bombed with takes away one Heart. When a player runs out of Hearts, the Tank is removed and the player becomes a Juror.  


CHERRY POINTS: Once per day, each player gains one Cherry. Cherries are spent to perform actions. Cherries can be spent during any break time in the day (dont let the game interfere with work time!). The Honor System is used here; players are responsible for managing their own action/point-spending behaviour!

CHERRY BOMBING: Players spend Cherries to Bomb other tanks near them on the board. Cherries can either be GOOD or BAD, shooter's choice. BAD Cherries take away Hearts from the Tank they target. GOOD Cherries donate Cherries to the target for them to spend.  
	RANGE: Cherries are shot over a range of board cells. The Max Range is 3. The effect of a Cherry Bomb is 1 per every cell of range. Bombing a Tank 1 cell away would cause 1 damage per Cherry spent; 1 damage per 2 Cherries spent at range 2, 1 damage per 3 Cherries spent at range 3. Replace "damage" with "donation" if bombing with Good cherries. Use Manhattan Range (diagonal cells count as 1 distance away)  

THE JURY: "Dead" players enter into a group of Jurors. Once per day, a Juror can vote for one existing Player to help. Every Player that recieves at least 3 votes from the Jury gets an extra Cherry.  

WINNING: The Last Tanks standing at the end of the Season are all winners, and get Trophies or someshit.  




SETUP:  

Day Zero: On the first day of work, each Player takes a Tank and labels it with their initials. 3 red dots represent their Hearts. Player then place their Tank on any cell they wish on the Board. Each Player then is given a Cup to keep their Cherries in, and their one Cherry point for the day.  

Doing Actions: Players can do as many actions as they like per day, as long as they have the Cherries for it. Actions must be written on the pad and placed in the box. You can write multiple actions on one page, or many pages throughout the day. Please make all writing LEGIBLE.
A simple code is used to write your actions and their resulting Cherry cost.  
All actions must include the Player's initials (the same as is labelled on their Tank).  

ACTIONS:  
Move:   "IW M B4 1"  tank IW Moves to cell B4, spending 1 Cherry  
BadBomb: "IW B TE 4" IW spends four Cherries to Bad Bomb TE  (CM determines Range and reduces TE's hearts by 4/range)  
GoodBomb: "IW G TE 3" IW spends three Cherries to donate to TE (CM determines range and drops 3/range caps in TE's cup)   
Juror Vote: "IW V TE" IW votes for TE to earn an extra Cherry for the day.   




